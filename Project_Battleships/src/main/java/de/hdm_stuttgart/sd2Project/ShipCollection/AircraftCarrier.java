package de.hdm_stuttgart.sd2Project.ShipCollection;

public class AircraftCarrier extends Ship {

    /**
     * Constructor for the ship type AIRCRAFT_CARRIER with two transfer parameters length and id.
     *
     * @param length       length of the ship.
     * @param id           unique ID of the ship.
     * @param healthPoints the Ships health(points)
     */
    AircraftCarrier(int length, String id, int healthPoints) {
        setLength(length);
        setId(id);
        setHealthPoints(healthPoints);
    }
}
