package de.hdm_stuttgart.sd2Project.ShipCollection;


import de.hdm_stuttgart.sd2Project.GuiControllers.GameStart;
import de.hdm_stuttgart.sd2Project.PlayerControl.ComputerPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.IPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public abstract class Ship implements IShip {

    private static final Logger log = LogManager.getLogger(Ship.class);
    private int[] shipPositions;
    private String id;
    private int length;
    private int healthPoints;

    /**
     * Method to set the shipPositions of a Ship, changes Field [] positions + sets
     * the positions in the field by calling the setFieldStatus Method of the Player Type
     *
     * @param demandedPositions where to set the ship
     * @param playerType        needed to change the right MatchField
     */
    @Override
    public boolean setPositions(int[] demandedPositions, IPlayer playerType) {
        boolean successfullySet = false;
        if (length != demandedPositions.length) {
            System.out.println("Field coordinates and Ship length differ!");
            log.debug("Ship " + id + "of " + playerType + "has wrong coordinate length for " + Arrays.toString(demandedPositions));
        } else {
            try {
                this.shipPositions = new int[demandedPositions.length];
                for (int i = 0; i < demandedPositions.length; i++) {
                    if (playerType instanceof HumanPlayer) {                                    // Bisher in jedem Schleifendurchgang abfrage nach Instanz sollte man noch ändern
                        ((HumanPlayer) playerType).getHumanField().setFieldStatus(demandedPositions[i], 1);      //Wenn 1 unbeschädigtes Schiff bedeutet
                    } else {
                        ((ComputerPlayer) playerType).getComputerField().setFieldStatus(demandedPositions[i], 1);
                    }
                    this.shipPositions[i] = demandedPositions[i];

                }

                successfullySet = true;

            } catch (ArrayIndexOutOfBoundsException positionNotInField) {
                log.debug("Coordinates " + Arrays.toString(demandedPositions) + " are not in the field!");
            }
        }
        return successfullySet;
    }


    /**
     * Method to rotate the positions of a ship. Checks if the input is correct and
     * that a another ship isn't hit. Returns true if Ship can be turned and changes the
     * ship coordinates + marks the spot in the field of the player
     */
    @Override
    public boolean rotateShip(IPlayer player) {
        int[] proposedPositions;
        int[] oldPositions;
        boolean shipCanBeTurned = false;
        boolean horizontal = this.shipPositions[1] - this.shipPositions[0] == 1;
        oldPositions = this.shipPositions;
        proposedPositions = Arrays.copyOf(shipPositions, shipPositions.length);
        for (int oldPosition : oldPositions) {
            ((HumanPlayer) player).getHumanField().setFieldStatus(oldPosition, ((HumanPlayer) player).getHumanField().getFieldStatus(oldPosition));
        }
        if (horizontal) {
            for (int i = 0; i < this.length; i++) {
                proposedPositions[i] = oldPositions[i] + (9 * i);
            }
        } else {
            if (oldPositions[0] % 10 == 0) {
                log.info("Ship " + id + " cannot be rotated");
                for (int oldPosition : oldPositions) {
                    ((HumanPlayer) player).getHumanField().setFieldStatus(oldPosition, ((HumanPlayer) player).getHumanField().getFieldStatus(oldPosition));
                }
                this.shipPositions = oldPositions;
            } else {
                for (int i = 0; i < this.length; i++) {
                    proposedPositions[i] = oldPositions[i] - (9 * i);
                }
            }
        }
        log.debug("Checking for rotation with " + Arrays.toString(proposedPositions));
        if (((HumanPlayer) player).checkInput(proposedPositions, player)) {
            for (int oldPosition : oldPositions) {
                ((HumanPlayer) player).getHumanField().setFieldStatus(oldPosition, ((HumanPlayer) player).getHumanField().getFieldStatus(oldPosition));
            }
            this.setShipPositions(proposedPositions);
            log.info(((HumanPlayer) player).getName() + " rotated Ship " + id + " new Positions: " + Arrays.toString(proposedPositions));
            shipCanBeTurned = true;
        } else {
            log.info("Ship " + id + " cannot get rotated on positions: " + Arrays.toString(proposedPositions));
            for (int oldPosition : oldPositions) {
                ((HumanPlayer) player).getHumanField().setFieldStatus(oldPosition, ((HumanPlayer) player).getHumanField().getFieldStatus(oldPosition));
            }
            this.shipPositions = oldPositions;

        }
        return shipCanBeTurned;
    }

    /**
     * Method to get HealthPoints of the ship.
     *
     * @return healthPoints.
     */
    public int getHealthPoints() {
        return healthPoints;
    }

    /**
     * Method to set HealthPoints of the ship.
     *
     * @param healthPoints the ship should have.
     */
    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    /**
     * Method to get Length of a the ship.
     *
     * @return length.
     */
    public int getLength() {
        return length;
    }

    /**
     * Method to set the Length of the ship.
     *
     * @param length the ship should have.
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Method to get the positions of a ship.
     *
     * @return positions.
     */
    public int[] getShipPositions() {
        return shipPositions;
    }

    /**
     * Method to set the positions of the ship.
     *
     * @param shipPositions the ship should have.
     */
    public void setShipPositions(int[] shipPositions) {
        this.shipPositions = shipPositions;
    }

    /**
     * Method to get the Id of a ship.
     *
     * @return id.
     */
    public String getId() {
        return id;
    }

    /**
     * Method to set the Id of a ship.
     *
     * @param id the ship should have.
     */
    public void setId(String id) {
        this.id = id;
    }


}
