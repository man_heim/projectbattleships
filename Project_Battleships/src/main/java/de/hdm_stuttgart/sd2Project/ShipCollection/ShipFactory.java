package de.hdm_stuttgart.sd2Project.ShipCollection;

import de.hdm_stuttgart.sd2Project.Exceptions.IllegalFactoryArgument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShipFactory {

    private static final Logger log = LogManager.getLogger(ShipFactory.class);
    private static int cruiserNumber = 0;
    private static int destroyerNumber = 0;
    private static int submarineNumber = 0;

    /**
     * Sets the id for the ships of type Cruiser.
     *
     * @param cruiserNumber is the id of the ship.
     */
    public static void setCruiserNumber(int cruiserNumber) {
        ShipFactory.cruiserNumber = cruiserNumber;
    }

    /**
     * Sets the id for the ships of type Cruiser.
     *
     * @param destroyerNumber is the id of the ship.
     */
    public static void setDestroyerNumber(int destroyerNumber) {
        ShipFactory.destroyerNumber = destroyerNumber;
    }

    /**
     * Sets the id for the ships of type Cruiser.
     *
     * @param submarineNumber is the id of the ship.
     */
    public static void setSubmarineNumber(int submarineNumber) {
        ShipFactory.submarineNumber = submarineNumber;
    }

    /**
     * Method to create Ships for the fleets of the players.
     *
     * @param type indicates which ship type should be created.
     * @return new instance(IShip) of the chosen ship.
     */
    public static IShip createShip(String type) throws IllegalFactoryArgument {

        switch (type.toUpperCase().replace(" ", "")) {

            case "DESTROYER":
                destroyerNumber++;
                log.debug("Create new Destroyer" + destroyerNumber);
                return new Destroyer(3, "Destroyer" + destroyerNumber, 3);

            case "SUBMARINE":
                submarineNumber++;
                log.debug("Create new Submarine" + submarineNumber);
                return new Submarine(2, "Submarine" + submarineNumber, 2);

            case "CRUISER":
                cruiserNumber++;
                log.debug("Create new Cruiser" + cruiserNumber);
                return new Cruiser(4, "Cruiser" + cruiserNumber, 4);

            case "AIRCRAFTCARRIER":
                log.debug("Create new AircraftCarrier");
                return new AircraftCarrier(5, "AircraftCarrier", 5);

            default:
                log.error(type + " is no type of a ship");
                throw new IllegalFactoryArgument("Wrong ship type!");
        }
    }
}
