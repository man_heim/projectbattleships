package de.hdm_stuttgart.sd2Project.ShipCollection;

public class Submarine extends Ship {

    /**
     * Constructor for the ship type SUBMARINE with two transfer parameters length and id.
     *
     * @param length length of the ship.
     * @param id     unique ID of the ship.
     */
    Submarine(int length, String id, int healthPoints) {
        setLength(length);
        setId(id);
        setHealthPoints(healthPoints);
    }
}
