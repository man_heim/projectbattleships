package de.hdm_stuttgart.sd2Project.ShipCollection;

import de.hdm_stuttgart.sd2Project.PlayerControl.IPlayer;

public interface IShip {

    boolean setPositions(int[] demandedPositions, IPlayer playerType);

    boolean rotateShip(IPlayer player);

}
