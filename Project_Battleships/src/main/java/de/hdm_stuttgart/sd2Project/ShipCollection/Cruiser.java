package de.hdm_stuttgart.sd2Project.ShipCollection;

public class Cruiser extends Ship {

    /**
     * Constructor for the ship type CRUISER with two transfer parameters length and id.
     *
     * @param length length of the ship.
     * @param id     unique ID of the ship.
     */
    Cruiser(int length, String id, int healthPoints) {
        setLength(length);
        setId(id);
        setHealthPoints(healthPoints);
    }
}
