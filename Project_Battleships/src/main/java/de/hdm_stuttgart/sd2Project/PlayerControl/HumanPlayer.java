package de.hdm_stuttgart.sd2Project.PlayerControl;

import de.hdm_stuttgart.sd2Project.MapControl.MatchField;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HumanPlayer extends Player {

    private static final Logger log = LogManager.getLogger(HumanPlayer.class);
    private Fleet humanFleet;
    private int numberOfSetShips = 0;
    private MatchField humanField;
    private String name = "dummyName";

    /**
     * Constructor to create a HumanPlayer.
     */
    public HumanPlayer() {
        this.humanFleet = new Fleet();
        this.humanField = new MatchField();
        log.info("New HumanPlayer and MatchField created");
    }

    /**
     * Method to get HumanField.
     *
     * @return humanField.
     */
    public MatchField getHumanField() {
        return new MatchField(humanField);
    }

    /**
     * Method getNumberOfSetShips.
     *
     * @return the number of the set ships.
     */
    public int getNumberOfSetShips() {
        return numberOfSetShips;
    }

    /**
     * Method set the number of the ships set.
     *
     * @param numberOfSetShips how many ships are set.
     */
    public void setNumberOfSetShips(int numberOfSetShips) {
        this.numberOfSetShips = numberOfSetShips;
    }

    /**
     * Method to set all Ships of the humanFleet
     * by calling the setPositions method from class Ship.
     *
     * @param demandedPositions where to set the ship.
     * @param shipId            Ship that should be set.
     */
    @Override
    public boolean setShips(int[] demandedPositions, String shipId) {
        boolean correctPosition = false;
        if (this.checkInput(demandedPositions, this)) {
            correctPosition =
                    humanFleet.getFleetList().stream()
                            .filter(ship -> ((Ship) ship).getId().equals(shipId))
                            .anyMatch(ship -> ship.setPositions(demandedPositions, this));
        }
        return correctPosition;
    }

    /**
     * Method to return the Fleet of the HumanPlayer
     *
     * @return humanFleet (ArrayList with Ships)
     */
    @Override
    public Fleet getFleet() {
        return new Fleet(humanFleet);
    }

    /**
     * Method to return the name of HumanPlayer.
     *
     * @return name of HumanPlayer.
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName sets the name of the HumanPlayer.
     *
     * @param name name the player inserted.
     */
    public void setName(String name) {
        this.name = name;
        log.info("You set the name to " + name);
    }


}
