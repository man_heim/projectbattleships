package de.hdm_stuttgart.sd2Project.PlayerControl;


import java.util.ArrayList;
import java.util.List;

public interface IPlayer {

    Fleet getFleet();


    int bombField(int fieldNumber, IPlayer player);

    boolean setShips(int[] demandedPositions, String shipId);

    void removeShipFromFleet(IPlayer player,IPlayer attackingPlayer);

    List<ITurret> getPlayersTurrets();

}
