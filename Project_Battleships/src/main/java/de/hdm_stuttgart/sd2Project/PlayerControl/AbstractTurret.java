package de.hdm_stuttgart.sd2Project.PlayerControl;

import java.util.ArrayList;
import java.util.Random;

public class AbstractTurret implements ITurret {

    private int range;

    private int position;
    private ArrayList<Integer> possibleFieldsToShoot = new ArrayList<>();

    public AbstractTurret(int position, int range) {
        this.position = position;
        this.range = range;
        evaluateFieldsToShoot();
    }


    @Override
    public ArrayList<Integer> getTurretRange() {
        return possibleFieldsToShoot;
    }

    /**
     * @return field which should be shot
     */
    @Override
    public int fieldToShoot() {
        int number;
        Random random = new Random();
        number = possibleFieldsToShoot.get(random.nextInt(possibleFieldsToShoot.size()));
        return number;
    }

    @Override
    public int getPosition() {
        return position;
    }

    /**
     * evaluating the fields that are in range and add them to a list
     */
    private void evaluateFieldsToShoot() {

        int firstRowStart = position - (range - 1) - ((range - 1) * 10);
        int lastRowStart = firstRowStart + (10 * (range * 2 - 2));
        int multiplier = position - (range - 1) * 10;
        if (multiplier < 0) multiplier = 0;
        int posHelper = position % (position / 10);

        if (position % (position / 10) < 7 && position%10!=0) {

            for (int x = firstRowStart; x <= lastRowStart; x += 10) {
                for (int y = 0; y < range * 2 - 1; y++) {
                    if (x + y < 101 && x + y > 0 && ((x + y) / 10) == multiplier / 10 && (x + y) % 10 != 0) {

                        possibleFieldsToShoot.add(x + y);
                    }
                }
                multiplier += 10;
            }

            if (position - range * 10 < 101 && position - range * 10 > 0) possibleFieldsToShoot.add(position - range * 10);
            if (position - range < 101 && position - range > 0 && ((position - range-1) / 10 == position / 10))
                possibleFieldsToShoot.add(position - range);
            if (position + range < 101 && position + range > 0 && (position + range) / 10 == position / 10)
                possibleFieldsToShoot.add(position + range);
        } else {
            for (int x = firstRowStart; x <= lastRowStart; x += 10) {
                for (int y = 0; y < range * 2 - 1; y++) {
                    if ((x + y) % 10 != 0 && (y+x)>7 && (y+x)<101) {
                        possibleFieldsToShoot.add(x + y);
                    } else if((y+x)>7 && (y+x)<101){
                        possibleFieldsToShoot.add(x+y);
                        break;
                    }
                }
                multiplier += 10;
            }
            if (position - range * 10 < 101 && position - range * 10 > 0) possibleFieldsToShoot.add(position - range * 10);
            possibleFieldsToShoot.add(position-range);
        }
        if (position + range * 10 < 101 && position + range * 10 > 0) possibleFieldsToShoot.add(position + range * 10);

    }
}
