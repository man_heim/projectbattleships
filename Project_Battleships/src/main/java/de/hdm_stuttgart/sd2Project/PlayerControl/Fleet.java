package de.hdm_stuttgart.sd2Project.PlayerControl;

import de.hdm_stuttgart.sd2Project.Exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import de.hdm_stuttgart.sd2Project.ShipCollection.ShipFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fleet {

    private static final Logger log = LogManager.getLogger(Fleet.class);
    /**
     * ArrayList Fleet containing all Ships.
     */
    private List<IShip> fleetList = new ArrayList<>();


    /**
     * Constructor to create a Fleet with a certain number of the different Ships.
     * Resets the Number of Ships needed to create a unique ID.
     */
    Fleet() {
        try {
            fleetList.add(ShipFactory.createShip("AircraftCarrier"));
            int cruiserCount = 2;
            for (int i = 0; i < cruiserCount; i++) {
                fleetList.add(ShipFactory.createShip("Cruiser"));
            }
            int destroyerCount = 3;
            for (int i = 0; i < destroyerCount; i++) {
                fleetList.add(ShipFactory.createShip("Destroyer"));
            }
            int submarineCount = 4;
            for (int i = 0; i < submarineCount; i++) {
                fleetList.add(ShipFactory.createShip("Submarine"));
            }
        } catch (IllegalFactoryArgument e) {
            log.error(e.toString());
        }

        ShipFactory.setDestroyerNumber(0);
        ShipFactory.setCruiserNumber(0);
        ShipFactory.setSubmarineNumber(0);
    }

    /**
     * Copy Constructor to return a Copy of the fleet!
     *
     * @param other original Fleet.
     */
    Fleet(Fleet other) {
        this.fleetList = other.fleetList;
    }

    /**
     * Method searchForShipToRotate is filtering the Fleet to see if a ship could be rotated.
     *
     * @param shipId of the ship, that should be rotated.
     * @param player indicates which Fleet should be used.
     * @return true if ship could be rotated.
     */
    public boolean searchForShipToRotate(String shipId, IPlayer player) {
        boolean shipFoundAndTurned = false;
        try {
            fleetList.stream()
                    .filter(ship ->
                            ((Ship) ship).getId().equals(shipId)
                    ).findAny()
                    .filter(ship ->
                            ship.rotateShip(player)
                    )
                    .orElseThrow(NoSuchElementException::new);
            shipFoundAndTurned = true;
        } catch (NoSuchElementException notTurnabel) {
            log.info("Ship " + shipId + "cannot be turned ");
        }
        return shipFoundAndTurned;
    }

    /**
     * Method to return the Fleet of a player.
     *
     * @return fleet of the player.
     */
    public List<IShip> getFleetList() {
        return fleetList;
    }
}
