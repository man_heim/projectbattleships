package de.hdm_stuttgart.sd2Project.PlayerControl;

import java.util.ArrayList;

public interface ITurret {
    int getPosition();
    int fieldToShoot();
    ArrayList<Integer> getTurretRange();
}
