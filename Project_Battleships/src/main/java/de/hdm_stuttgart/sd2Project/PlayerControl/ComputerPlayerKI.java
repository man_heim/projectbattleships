package de.hdm_stuttgart.sd2Project.PlayerControl;

import de.hdm_stuttgart.sd2Project.GuiControllers.GameLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ComputerPlayerKI {


    private final static Logger log = LogManager.getLogger(ComputerPlayerKI.class);

    private boolean horizontal=true;            //Computer should try to shoot horizontal first
    private int sideHorizontal = 0;            //start =0, 1 = shoot right, -1 = shoot left
    private int sideVertical = 0;                //start =0 , 1 = up, -1 = down
    private int initialShot = -1;               //only when hit, otherwise -1
    private int hitCounter=0;
    private List<Integer> availableNumbers = new ArrayList<>();

    public ComputerPlayerKI() {
        for (int i = 1; i < 101; i++) {
            availableNumbers.add(i);
        }
    }

    int shootField() {
        int shotToReturn = 0;

        if (initialShot == -1) {
            int number;
            Random random = new Random();
            number = availableNumbers.get(random.nextInt(availableNumbers.size()));
            availableNumbers.remove((Integer) number);
            if (GameLogic.player.getHumanField().getFieldStatus(number) == 1)
                initialShot = number;
            shotToReturn = number;
        } else {
            if (horizontal) {
                if (initialShot % 10 != 0 && (sideHorizontal == 0 || sideHorizontal == 1)) {
                    int number = initialShot + 1;
                    if (GameLogic.player.getHumanField().getFieldStatus(number) == 1) {
                        initialShot++;
                        hitCounter++;
                    } else {
                        sideHorizontal = -1;

                    }
                    shotToReturn = number;
                } else if (initialShot % 10 != 1 && (sideHorizontal == 0 || sideHorizontal == -1)) {
                    int number = (initialShot - 1-hitCounter);
                    if (GameLogic.player.getHumanField().getFieldStatus(number) == 1) {
                        initialShot--;
                    } else {
                        horizontal = false;
                    }
                    return number;
                }

            } else {
                if (initialShot - 10 > 0 && (sideVertical == 0 || sideVertical == 1)) {
                    int number = initialShot - 10;
                    if (GameLogic.player.getHumanField().getFieldStatus(number) == 1) {
                        initialShot -= 10;
                        hitCounter++;
                    } else {
                        sideVertical = -1;
                    }
                    shotToReturn = number;
                } else if (initialShot + 10 < 100 && (sideVertical == 0 || sideVertical == -1)) {
                    int number = (initialShot +10*hitCounter + 10);
                    if (GameLogic.player.getHumanField().getFieldStatus(number) == 1) {
                        initialShot += 10;
                    } else {
                        sideVertical = 0;
                    }
                    shotToReturn = number;
                }
            }
        }
        log.debug("Field to be shot " + shotToReturn + " inital Shot: " + initialShot);
        availableNumbers.remove(Integer.valueOf(shotToReturn));
        return shotToReturn;
    }

    void reset() {
        horizontal = true;
        sideVertical = 0;
        sideHorizontal = 0;
        initialShot = -1;
        hitCounter=0;
        log.debug("Resetted all KI Helper Properties");
    }

}
