package de.hdm_stuttgart.sd2Project.PlayerControl;

import java.util.ArrayList;

public class StandardTurret extends AbstractTurret{

    //Standard Turret has range 3
    StandardTurret(int position){
       super(position,3);
    }
}