package de.hdm_stuttgart.sd2Project.PlayerControl;

import de.hdm_stuttgart.sd2Project.GuiControllers.MatchFieldController;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Player implements IPlayer {

    private static final Logger log = LogManager.getLogger(Player.class);
    private boolean playerWin;
    private boolean computerWin;
    private int bombCounter = 0;
    private int points=0;

    private List<ITurret> turretList = new ArrayList<>();

    /**
     * Method getBombCounter,
     *
     * @return the bombCounter of the HumanPlayer.
     */
    public int getBombCounter() {
        return bombCounter;
    }


    @Override
    public List<ITurret> getPlayersTurrets() {
        return turretList;
    }


    /**
     * Method setBombCounter sets the bombCounter to the counterSet.
     *
     * @param counterSet the amount of shots the player shot.
     */
    public void setBombCounter(int counterSet) {
        bombCounter = counterSet;
    }

    /**
     * returns the value of Computer Win as a Copy
     *
     * @return copyOf Computer Win
     */
    public boolean isComputerWin() {
        return computerWin;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Sets computerWin
     *
     * @param computerWin value to set
     */
    public void setComputerWin(boolean computerWin) {
        this.computerWin = computerWin;
    }

    /**
     * returns the value of Player Win as a Copy
     *
     * @return copyOf Player Win
     */
    public boolean isPlayerWin() {
        return playerWin;
    }



    /**
     * setShips is implemented slightly different in HumanPlayer or ComputerPlayer
     *
     * @param demandedPositions where to set the ships
     * @param shipId            which ship to set
     */
    abstract public boolean setShips(int[] demandedPositions, String shipId);

    /**
     * Method bombField is the shoot of the player at one of the Field in the matchfield.
     * It changes the fieldStatus if it is Water or Ship.
     * You cannot shoot the same Field twice.
     *
     * @param fieldNumber which Field should be shot.
     */
    @Override
    public int bombField(int fieldNumber, IPlayer player) {

        int shot = -1;


        if (fieldNumber >= 1 && fieldNumber <= 100) {

            if (player instanceof ComputerPlayer) { //Human shoots computer Field

                if (((ComputerPlayer) player).getComputerField().getFieldStatus(fieldNumber) == 0) {//Wasser wird zu Daneben!

                    ((ComputerPlayer) player).getComputerField().setFieldStatus(fieldNumber, 3);
                    bombCounter++;
                    shot = 3;

                } else if (((ComputerPlayer) player).getComputerField().getFieldStatus(fieldNumber) == 1) {//Schiff wird zu Treffer!

                    ((ComputerPlayer) player).getComputerField().setFieldStatus(fieldNumber, 2);
                    bombCounter++;
                    reduceHealthPoints(fieldNumber, player);
                    removeShipFromFleet(player,this);
                    shot = 2;

                } else if (((ComputerPlayer) player).getComputerField().getFieldStatus(fieldNumber) == 3 || ((ComputerPlayer) player).getComputerField().getFieldStatus(fieldNumber) == 2) { //Treffer und Daneben kann nicht nochmal beschossen werden.
                    log.info("HumanPlayer tried to shoot a field that he already shot");

                }
            } else if (player instanceof HumanPlayer) {         //Computer shoots HumanField
                if (((HumanPlayer) player).getHumanField().getFieldStatus(fieldNumber) == 0) {//Wasser wird zu Daneben!

                    ((HumanPlayer) player).getHumanField().setFieldStatus(fieldNumber, 3);
                    shot = 3;

                } else if (((HumanPlayer) player).getHumanField().getFieldStatus(fieldNumber) == 1) {//Schiff wird zu Treffer!

                    ((HumanPlayer) player).getHumanField().setFieldStatus(fieldNumber, 2);
                    reduceHealthPoints(fieldNumber, player);
                    removeShipFromFleet(player, this);
                    shot = 2;

                } else if (((HumanPlayer) player).getHumanField().getFieldStatus(fieldNumber) == 3 || ((HumanPlayer) player).getHumanField().getFieldStatus(fieldNumber) == 2) { //Treffer und Daneben kann nicht nochmal beschossen werden.
                    log.info("ComputerPlayer tried to shoot a field that he already shot");
                }
            }
        } else {
            System.out.println("You have to shoot a field that exists!");

        }
        return shot;
    }

    /**
     * Method reduceHealthPoints is going through every position of a ship and compares the fieldNumber with
     * the shot Field. If it is correct it will reduce the healthPoints of this ship by one.
     * This method called in the bombField method.
     *
     * @param fieldNumber which Field is shoot.
     * @param player      which Fleet should be searched.
     */
    private void reduceHealthPoints(int fieldNumber, IPlayer player) {
        OUTER_LOOP:
        for (IShip s : player.getFleet().getFleetList()) {
            for (int i = 0; i < ((Ship) s).getShipPositions().length; i++) {
                if (((Ship) s).getShipPositions()[i] == fieldNumber) {
                    ((Ship) s).setHealthPoints(((Ship) s).getHealthPoints() - 1);
                    break OUTER_LOOP;
                }
            }
        }
    }

    /**
     * Checks the input, where the player wants to set ships, gets called in setShips
     * Checks if the order of position makes sense, calls a method that checks if the spot and spots around
     * are already occupied by another ship
     *
     * @param demandedPositions positions, where the player wants to set the ship
     * @return if the Player input is correct or not
     */
    public boolean checkInput(int[] demandedPositions, IPlayer player) {
        boolean testedInput = true;
        boolean horizontal = demandedPositions[1] - demandedPositions[0] == 1;
        for (int i = 1; i < demandedPositions.length; i++) {
            if (demandedPositions[i] - demandedPositions[i - 1] != 1 && demandedPositions[i] - demandedPositions[i - 1] != 10 || demandedPositions[0] < 1 || demandedPositions[demandedPositions.length - 1] > 100) {
                testedInput = false;
            }
        }
        if (horizontal) {
            if (demandedPositions[0] % 10 == 0) {
                testedInput = false;
            } else {
                int divider = demandedPositions[0] / 10;
                for (int j : demandedPositions) {
                    if (j / 10 != divider) {
                        if (j % 10 != 0) {
                            testedInput = false;
                            break;
                        }
                    }
                }
            }
        } else {
            int divider = demandedPositions[0] % 10;
            for (int j : demandedPositions) {
                if (j % 10 != divider) {
                    testedInput = false;
                    break;
                }
            }
        }
        if (!testedInput) {
            log.debug("Can't choose these field.!");
        } else {
            if (player instanceof HumanPlayer) {
                log.debug("checking for nearby ship with coordinates " + Arrays.toString(demandedPositions));
                if (!(((HumanPlayer) player).getHumanField().checkForNearbyShip(horizontal, demandedPositions))) {
                    log.debug("There is a Ship nearby testedInput");
                    testedInput = false;
                }
            } else if (player instanceof ComputerPlayer) {
                if (!(((ComputerPlayer) player).getComputerField().checkForNearbyShip(horizontal, demandedPositions))) {
                    testedInput = false;
                }
            }
        }
        log.debug("testedInput= " + testedInput);
        return testedInput;
    }

    /**
     * Creates a turret on a position, 2nd parameter type of turret
     * @param turretPosition position of turret
     */
    public void createTurret(String turretPosition){
        turretList.add(TurretFactory.createTurret(turretPosition,"std"));
        log.info("Human Player set a turret at position "+turretPosition );
    }



    /**
     * Method removeShipFromFleet checks which ship has no healthPoints left and removes it from the Fleet.
     * Sets boolean if the fleet has no ships left.
     *
     * @param player from which Fleet the ship should removed.
     */
    public void removeShipFromFleet(IPlayer player,IPlayer attackingPlayer) {                          //TODO removeShipFromFleet soll Schiff returnen, das zerstört wurde (oder ruft methode auf, die dieses schiff aufnimmt und in der Gui erkenntlich macht + Punkte erhöhen!
        if (player instanceof HumanPlayer) {
            player.getFleet().getFleetList().stream()
                    .filter(ship -> ((Ship) ship).getHealthPoints() == 0)
                    .findFirst()
                    .ifPresent(ship -> {
                      //  ((Ship) ship).shipHasDied(attackingPlayer);
                        player.getFleet().getFleetList().remove(ship);      //Computer Player destroyed a ship of Human Player //TODO letzte gespeicherte Schüsse von KI können zurück gesetzt werden
                        log.info("Ship " + ((Ship) ship).getId() + " of " + ((HumanPlayer) player).getName() + " has been destroyed");
                        ((ComputerPlayer) attackingPlayer).setPoints(((ComputerPlayer) attackingPlayer).getPoints()+((Ship) ship).getLength()/2);
                        ((ComputerPlayer) attackingPlayer).getComputerPlayerKI().reset();
                    });
            if (player.getFleet().getFleetList().size() == 0) {
                computerWin = true;
            }
        } else {
            player.getFleet().getFleetList().stream()
                    .filter(ship -> ((Ship) ship).getHealthPoints() == 0)
                    .findFirst()
                    .ifPresent(ship -> {
                        player.getFleet().getFleetList().remove(ship);
                        log.info("Ship " + ((Ship) ship).getId() + " of ComputerPlayer has been destroyed");
                        ((HumanPlayer) attackingPlayer).setPoints(((HumanPlayer) attackingPlayer).getPoints()+((Ship) ship).getLength()/2);
                    });
            if (player.getFleet().getFleetList().size() == 0) {
                playerWin = true;
            }
        }
    }
}
//TODO Player bekommt Liste aus Turrets, diese müssen jeweils Bereich ausrechnen, den sie abschießen können + Schuss Methode haben bzw. auf Schuss Methode zugreifen
//TODO Turret hat Schussbereich, beim Schießen Player ruft Methode auf, die turrets einzeln schießen lässt und ein zufälliges Feld trifft evtl. später smarte Turrets