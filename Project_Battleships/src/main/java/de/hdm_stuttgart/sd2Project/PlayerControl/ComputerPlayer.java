package de.hdm_stuttgart.sd2Project.PlayerControl;

import de.hdm_stuttgart.sd2Project.MapControl.MatchField;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ComputerPlayer extends Player {

    private static final Logger log = LogManager.getLogger(ComputerPlayer.class);
    private Fleet npcFleet;
    private MatchField computerField;
    private int shipNumber;
    private ComputerPlayerKI computerPlayerKI;


    public ComputerPlayerKI getComputerPlayerKI() {
        return computerPlayerKI;
    }

    /**
     * Constructor to create a new ComputerPlayer with his own matchfield.
     */
    public ComputerPlayer() {
        this.computerField = new MatchField();
        this.npcFleet = new Fleet();
        log.info("New ComputerPlayer and MatchField created");

        computerPlayerKI = new ComputerPlayerKI();
    }

    /**
     * Method which return the ComputerField.
     *
     * @return copy of computerField.
     */
    public MatchField getComputerField() {
        return new MatchField(computerField);
    }

    /**
     * Method getShipNumber.
     *
     * @return the shipNumber.
     */
    private int getShipNumber() {
        return shipNumber;
    }

    /**
     * Method to set all Ships of the npcFleet
     * by calling the setPositions method from class Ship.
     *
     * @param demandedPosition where to set the ship.
     * @param shipId           Ship that should be set.
     */
    @Override
    public boolean setShips(int[] demandedPosition, String shipId) {
        boolean correctShipPosition = false;
        for (IShip s : npcFleet.getFleetList()) {
            if (((Ship) s).getId().equals(shipId)) {
                Random random = new Random();
                int vertOrHoriz = random.nextInt(2);
                int[] demandedPositions = new int[((Ship) s).getLength()];
                demandedPositions[0] = demandedPosition[0];
                for (int i = 1; i < ((Ship) s).getLength(); i++) {
                    demandedPositions[i] = demandedPosition[0] + i * (vertOrHoriz * 9 + 1);
                }
                if ((this).checkInput(demandedPositions, this)) {
                    s.setPositions(demandedPositions, this);
                    shipNumber++;
                    correctShipPosition = true;
                    log.info("Ship " + shipId + " of ComputerPlayer successfully set at " + Arrays.toString(demandedPositions));
                }
                break;
            }
        }
        return correctShipPosition;
    }

    /**
     * Method to return the Fleet of the ComputerPlayer
     *
     * @return npcFleet (ArrayList with Ships)
     */
    @Override
    public Fleet getFleet() {
        return new Fleet(npcFleet);
    }

    /**
     * Method generateNumber is choosing a random number between 1 and 100.
     *
     * @return number which is generated.
     */
    public int chooseFieldToShoot() {

        return computerPlayerKI.shootField();           //TODO timer um schüsse zu verzögern
    }

    /**
     * Method npcSetShips sets the ships of the computerPlayer at random Fields.
     */
    public void npcSetShips() {
        do {
            Random random = new Random();
            int randomShipStartPosition = random.nextInt(99) + 1;
            this.setShips(new int[]{randomShipStartPosition}, ((Ship) this.getFleet().getFleetList().get(this.getShipNumber())).getId());
        } while (this.getFleet().getFleetList().size() > this.getShipNumber());
    }
}
