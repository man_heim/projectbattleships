package de.hdm_stuttgart.sd2Project.PlayerControl;

public class TurretFactory {

    /**
     * Creates turrets
     * @param turretPosition position of the turret
     * @param turretType type of the turret
     * @return returns the turret to be used in a list of turrets of a player
     */
    public static ITurret createTurret(String turretPosition, String turretType){
        switch (turretType){
            case "std": return new StandardTurret(Integer.parseInt(turretPosition));
            case "special":
                System.out.println("Not yet implemented"); //For special Turrets
                return new StandardTurret(Integer.parseInt(turretPosition));
            default: return new StandardTurret(Integer.parseInt(turretPosition)); //Later not default
        }
    }
}
