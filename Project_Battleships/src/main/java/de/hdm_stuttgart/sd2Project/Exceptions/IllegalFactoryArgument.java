package de.hdm_stuttgart.sd2Project.Exceptions;

public class IllegalFactoryArgument extends Exception {

    public IllegalFactoryArgument(String message) {
        super(message);
    }
}