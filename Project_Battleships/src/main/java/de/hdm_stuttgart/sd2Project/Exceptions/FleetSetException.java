package de.hdm_stuttgart.sd2Project.Exceptions;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FleetSetException extends Exception {

    private static final Logger log = LogManager.getLogger(FleetSetException.class);

    public FleetSetException(String message) {
        super(message);
        log.debug("The Player set all his ships");
        log.info("You set all your ships");
    }
}
