package de.hdm_stuttgart.sd2Project.MapControl;

public class Field {

    private int fieldNumber;
    private int status;         //Water 0, Ship 1, Missed = 3, Hit = 2

    /**
     * Constructor to create a Field with this fieldNumber.
     *
     * @param fieldNumber unique ident of a Field.
     */
    Field(int fieldNumber) {
        this.fieldNumber = fieldNumber;
    }

    /**
     * Method getFieldNumber
     * @return the number of the Field.
     */
    public int getFieldNumber() {
        return fieldNumber;
    }

    /**
     * Method to return the status of a Field.
     *
     * @return status of the Field.
     */
    int getStatus() {
        return status;
    }

    /**
     * Method to set status of a Field.
     *
     * @param status indicates if the Field is a ship, water, etc.
     */
    void setStatus(int status) {
        this.status = status;
    }


}