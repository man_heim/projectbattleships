package de.hdm_stuttgart.sd2Project.MapControl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.stream.Stream;

public class MatchField {

    private static final Logger log = LogManager.getLogger(MatchField.class);
    /**
     * Game Field of size 10x10 consisting of Fields
     */
    private Field[][] matchField = new Field[10][10];

    /**
     * Constructor to fill the matchField with Fields (Objects/instances of class Field)
     * One Thread fills the fields 0-50, the other one the fields 51-100 and adding every Field a unique fieldNumber.
     * Human Matchfield and Computer Matchfield are created parallel -> 2x(Thread2 and Thread1) in LogFile
     */
    public MatchField() {
        long startTime = System.currentTimeMillis();
        Thread t1 = new Thread(new LowFields());
        Thread t2 = new Thread(new HighFields());
        t2.start();
        t1.start();
        try {
            t1.join();
            t2.join();
            log.debug("Took " + (System.currentTimeMillis() - startTime) + " ms");
        } catch (InterruptedException i) {
            log.error(i.getMessage());
        }
    }

    /**
     * Copy Constructor to return a copy of the MatchField!
     *
     * @param other original MatchField.
     */
    public MatchField(MatchField other) {
        this.matchField = other.matchField;
    }

    /**
     * Method to return the MatchField of the player.
     *
     * @return matchField of HumanPlayer.
     */
    public Field[][] getMatchField() {
        return matchField;
    }

    /**
     * @param start      with which index of a field to start
     * @param stop       with which index of a field to stop
     * @param threadName Name of the thread calling the method (for debugging purpose)
     */
    private void createFields(int start, int stop, String threadName) {
        log.debug(threadName+" started creating fields");
        int counter = start * 10 + 1;
        for (int i = start; i < stop; i++) {
            for (int j = 0; j < 10; j++) {
                matchField[i][j] = new Field(counter);
                log.trace("new Field " + counter + " created by " + threadName);
                counter++;
            }
        }
        log.debug(threadName + " finished creating fields");
    }

    /**
     * Method to return the field status of a Field by calling the getFieldStatus Method
     * in class Field.
     *
     * @param fieldNumber number of the chosen Field.
     * @return status of Field.
     */
    public int getFieldStatus(int fieldNumber) {
        int xCoordinate, yCoordinate;
        if (fieldNumber % 10 == 0) {
            yCoordinate = (fieldNumber / 10) - 1;
            xCoordinate = 10;
        } else {
            xCoordinate = fieldNumber % 10;
            yCoordinate = fieldNumber / 10;
        }
        log.trace(fieldNumber + "has Status " + matchField[yCoordinate][xCoordinate - 1].getStatus());
        return matchField[yCoordinate][xCoordinate - 1].getStatus();
    }

    /**
     * Method to set the status of a Field by calling the setStatus Method
     * in class Field.
     *
     * @param fieldNumber number of chosen Field.
     * @param status      status of the Field.
     * @throws ArrayIndexOutOfBoundsException because of the matchField Array.
     */
    public void setFieldStatus(int fieldNumber, int status) throws ArrayIndexOutOfBoundsException {
        int xCoordinate, yCoordinate;
        if (fieldNumber % 10 == 0) {
            yCoordinate = (fieldNumber / 10) - 1;
            xCoordinate = 10;
        } else {
            xCoordinate = fieldNumber % 10;
            yCoordinate = fieldNumber / 10;
        }
        matchField[yCoordinate][xCoordinate - 1].setStatus(status);

    }

    /**
     * Checks if the positions and the positions around the intended spots are already occupied by another ship
     *
     * @param horizontal        is the ship positioned horizontally or not
     * @param demandedPositions the positions the player chose
     * @return whether the demanded Positions are allowed
     */
    public boolean checkForNearbyShip(boolean horizontal, int[] demandedPositions) {
        boolean testedInput = true;
        int[] temp;
        int border;   //Wenn 0 dann keine 1 links 2 rechts
        if (horizontal) {
            if (demandedPositions[demandedPositions.length - 1] % 10 == 0) {       //Border right
                temp = Arrays.copyOf(demandedPositions, (demandedPositions.length) * 3 + 1);
                border = 2;
            } else if (demandedPositions[0] % 10 == 1) {                           //Border left
                border = 1;
                temp = Arrays.copyOf(demandedPositions, (demandedPositions.length) * 3 + 1);
            } else {                                                        //Keine Border an den Seiten
                temp = Arrays.copyOf(demandedPositions, (demandedPositions.length * 3) + 2);
                border = 0;
            }
            Stream.iterate(0, i -> i < (temp.length / 3), i -> i + 1)
                    .forEach(integer -> {
                        temp[integer + (temp.length / 3)] = temp[integer] - 10;
                        temp[integer + (2 * (temp.length / 3))] = temp[integer] + 10;
                    });
            switch (border) {
                case 0:
                    temp[temp.length - 2] = demandedPositions[0] - 1;
                    temp[temp.length - 1] = demandedPositions[demandedPositions.length - 1] + 1;
                    break;
                case 1:
                    temp[temp.length - 1] = demandedPositions[demandedPositions.length - 1] + 1;
                    break;
                case 2:
                    temp[temp.length - 1] = (demandedPositions[0] - 1);
                    break;
            }
        } else {
            if (demandedPositions[0] % 10 == 0) {                   //border right
                temp = Arrays.copyOf(demandedPositions, (demandedPositions.length * 2) + 2);
                Stream.iterate(0, i -> i < demandedPositions.length, i -> i + 1)
                        .forEach(integer -> temp[integer + (temp.length / 2) - 1] = demandedPositions[integer] - 1);
                temp[temp.length - 2] = demandedPositions[0] - 10;         //border left
                temp[temp.length - 1] = demandedPositions[demandedPositions.length - 1] + 10;
            } else if (demandedPositions[0] % 10 == 1) {
                temp = Arrays.copyOf(demandedPositions, (demandedPositions.length * 2) + 2);
                Stream.iterate(0, i -> i < demandedPositions.length, i -> i + 1)
                        .forEach(integer -> temp[integer + (temp.length / 2) - 1] = demandedPositions[integer] + 1);
                temp[temp.length - 2] = demandedPositions[0] - 10;
                temp[temp.length - 1] = demandedPositions[demandedPositions.length - 1] + 10;
            } else {                                            // no border
                temp = Arrays.copyOf(demandedPositions, (demandedPositions.length * 3) + 2);
                Stream.iterate(0, i -> i < demandedPositions.length, i -> i + 1)
                        .forEach(integer -> {
                            temp[integer + (temp.length / 3)] = demandedPositions[integer] - 1;
                            temp[integer + (2 * (temp.length / 3))] = demandedPositions[integer] + 1;
                        });
                temp[temp.length - 1] = demandedPositions[0] - 10;
                temp[temp.length - 2] = demandedPositions[demandedPositions.length - 1] + 10;
            }
        }
        for (int i : temp) {
            if (i > 0 && i <= 100) {
                if (this.getFieldStatus(i) == 1) {
                    testedInput = false;
                    log.trace("Ship set too close to another ship");
                    break;
                }
            }
        }
        log.trace("Allowing to set the fields: " + testedInput);
        return testedInput;
    }

    /**
     * The Thread, that creates the low fields
     */
    private class LowFields implements Runnable {
        @Override
        public void run() {
            createFields(0, 5, "thread1");
        }
    }

    /**
     * The Thread, that creates the high fields
     */
    private class HighFields implements Runnable {
        @Override
        public void run() {
            createFields(5, 10, "thread2");
        }
    }
}