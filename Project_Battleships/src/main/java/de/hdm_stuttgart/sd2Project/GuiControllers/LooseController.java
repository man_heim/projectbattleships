package de.hdm_stuttgart.sd2Project.GuiControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class LooseController implements Initializable {

    private static final Logger log = LogManager.getLogger(LooseController.class);

    @FXML
    Button mainMenu;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * Method changeToMainMenu will change the scene to the menu scene.
     * Is resetting the boolean if play or npc win.
     **/
    @FXML
    private void changeToMainMenu() {
        GameLogic.npc.setComputerWin(false);
        GameStart.getApplication().setScene("/fxml/menu.fxml", "Battleship");
        log.info("Changed scene back to main menu!");
    }
}
