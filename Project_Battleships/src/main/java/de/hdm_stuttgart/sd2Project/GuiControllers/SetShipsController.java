package de.hdm_stuttgart.sd2Project.GuiControllers;

import de.hdm_stuttgart.sd2Project.Exceptions.FleetSetException;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.*;


public class SetShipsController implements Initializable {

    private static final Logger log = LogManager.getLogger(SetShipsController.class);
    @FXML
    ToggleGroup direction;
    private Set<Label> labelCollection = new HashSet<>();
    private int[] markedCoordinates;
    private boolean rotated = false;
    @FXML
    private GridPane gridPane3;
    @FXML
    private Button nextButton;
    @FXML
    private Button mainMenu;
    @FXML
    private Button menuYes;
    @FXML
    private Button menuNo;
    @FXML
    private ToolBar toolBar;
    @FXML
    private Label menuField;
    @FXML
    private RadioButton rb1;
    @FXML
    private Button nextShip;
    @FXML
    private Button rotate;
    @FXML
    private Label ShipToSet;
    @FXML
    private Label display;
    @FXML
    private Label shipLength;

    private IShip currentShipToSet = (GameLogic.player.getFleet().getFleetList().get(GameLogic.player.getNumberOfSetShips()));


    /**
     * Method fieldClicked is called when a label in the gridpane is clicked with the mouse.
     * It will call the method choosingTheShipCoordinates to set the coordinates of the ship.
     * It hands over the labelNumber and the boolean.
     *
     * @param labelNumber which label is clicked.
     * @param horizontal  indicates if the ship should be set horizontal or vertical.
     */
    private void fieldClicked(String labelNumber, boolean horizontal) {
        if (rotated) {
            for (int i : markedCoordinates) {
                GameLogic.player.getHumanField().setFieldStatus(i, GameLogic.player.getHumanField().getFieldStatus(i));
            }
        }
        markedCoordinates = null;
        nextShip.setDisable(false);
        try {
            setCurrentShipToSet();
            rotate.setDisable(false);
            choosingTheShipCoordinates(currentShipToSet, Integer.parseInt(labelNumber), horizontal);
        } catch (FleetSetException fleetFull) {
            display.setText("Please Go to the Game");
            nextShip.setDisable(true);
        }
    }

    /**
     * Method choosingTheShipCoordinates is choosing the coordinates of the called ship
     * if it is possible. And will store the positions in markedCoordinates.
     *
     * @param ship             which ship should be set.
     * @param headerCoordinate is the head of the ship (clicked label).
     * @param horizontal       indicates if the ship should be set horizontal or vertical.
     * @throws FleetSetException when there are no more Ships to set
     */
    private void choosingTheShipCoordinates(IShip ship, int headerCoordinate, boolean horizontal) throws FleetSetException {
        markedCoordinates = new int[((Ship) ship).getLength()];
        if (horizontal) {
            for (int i = 0; i < ((Ship) ship).getLength(); i++) {
                markedCoordinates[i] = headerCoordinate + i;
            }
        } else {
            for (int i = 0; i < ((Ship) ship).getLength(); i++) {
                markedCoordinates[i] = headerCoordinate + (i * 10);                     //Positions are marked
            }
        }
        display.setText("You want to set your ship at: " + Arrays.toString(markedCoordinates));
        log.info("Player " + GameLogic.player.getName() + " wants to set his ships at " + Arrays.toString(markedCoordinates));
        try {
            Callable <Boolean> controllPostions = new PositionController(markedCoordinates,((Ship) ship).getId());
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            Future <Boolean>  positionsCorrect = executorService.submit(controllPostions);
            if (positionsCorrect.get()) {
                helperFunction(ship, markedCoordinates);
            } else {
                display.setText("You cant choose these coordinates");
                nextShip.setDisable(true);
                labelCollection.stream()
                        .filter(label -> label.getId().equals("marked"))
                        .forEach(label -> label.setId("stdLabel2"));
            }
        } catch (IndexOutOfBoundsException i) {
            throw new FleetSetException("No more Ships to set, all ships set");
        }catch (Exception callerException){
            log.fatal(callerException.getMessage());
        }
    }

    private class PositionController implements Callable<Boolean>{
        private int []markedCoordinates;
        private String shipID;
        PositionController(int [] markedCoordinates,String shipID ){
            this.markedCoordinates = markedCoordinates;
            this.shipID = shipID;
        }
        @Override
        public Boolean call() throws Exception {
            return GameLogic.player.setShips(markedCoordinates,shipID);
        }
    }

    /**
     * Method helperFunction is changing the FieldStatus of the marked Fields.
     *
     * @param ship              indicates which ship positions status should be changed.
     * @param markedCoordinates which Field fieldStatus should be changed.
     */
    private void helperFunction(IShip ship, int[] markedCoordinates) {
        ((Ship) ship).setShipPositions(null);
        for (int i : markedCoordinates) {
            GameLogic.player.getHumanField().setFieldStatus(i, 0);
        }
        setMarker(markedCoordinates, false);
    }

    /**
     * Method setMarker is changing the id for every label where a ship should be set.
     * If the boolean is true it will change the id to final.
     *
     * @param coordinatesToMark which coordinates should be marked.
     * @param loggedIn          indicates if the ship should be set or only marked.
     */
    private void setMarker(int[] coordinatesToMark, boolean loggedIn) {
        labelCollection.stream()
                .filter(label -> label.getId().equals("marked"))
                .forEach(label -> label.setId("stdLabel2"));
        if (loggedIn) {
            for (int i : coordinatesToMark) {
                labelCollection.stream()
                        .filter(label -> Integer.parseInt(label.getText()) == i)
                        .findFirst()
                        .ifPresent(label -> label.setId("setFinal"));
            }
        } else {
            for (int i : coordinatesToMark) {
                labelCollection.stream()
                        .filter(label -> Integer.parseInt(label.getText()) == i)
                        .findFirst()
                        .ifPresent(label -> label.setId("marked"));
            }
        }
    }

    /**
     * Method toNextShip will set the ship and choose the next ship for the player.
     * When all ships are set it will disable all buttons.
     *
     * @param actionEvent needed for the method.
     * @throws IndexOutOfBoundsException if the markedCoordinates unequal the length of the ship.
     */
    @FXML
    private void toNextShip(ActionEvent actionEvent) throws FleetSetException {
        currentShipToSet.setPositions(markedCoordinates, GameLogic.player);
        GameLogic.player.setNumberOfSetShips(GameLogic.player.getNumberOfSetShips() + 1);
        ShipToSet.setText(((Ship) currentShipToSet).getId());
        try {
            setCurrentShipToSet();
        } catch (FleetSetException fleetFull) {
            log.info(fleetFull.getMessage());

        }
        ShipToSet.setText(((Ship) currentShipToSet).getId());
        shipLength.setText(Integer.toString(((Ship) currentShipToSet).getLength()));
        setMarker(markedCoordinates, true);
        display.setText("You set your ship at: " + Arrays.toString(markedCoordinates));
        nextShip.setDisable(true);
        log.info("Player " + GameLogic.player.getName() + " has set his ship at coordinates " + Arrays.toString(markedCoordinates));
        rotate.setDisable(true);
    }

    /**
     * Is setting the currentShipToSet to the actual ship, that should be set.
     *
     * @throws FleetSetException if all ships are set!
     */
    private void setCurrentShipToSet() throws FleetSetException {
        try {
            this.currentShipToSet = GameLogic.player.getFleet().getFleetList().get(GameLogic.player.getNumberOfSetShips());
        } catch (IndexOutOfBoundsException fleetSet) {
            display.setText("All ships are set you can start now");
            ShipToSet.setText("All ships set!");
            nextShip.setDisable(true);
            rotate.setDisable(true);
            gridPane3.setDisable(true);
            nextButton.setDisable(false);
            throw new FleetSetException("No more ships to set, all ships set");
        }
    }

    /**
     * Method initialize is adding a label for every pane in the gridpane and will activate the method fieldClicked
     * for every label.
     *
     * @param url            is needed for initialize.
     * @param resourceBundle is needed for initialize.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (int i = 0; i < 100; i += 10) {
            gridPane3.addRow(i);
            for (int j = 1; j < 11; j++) {
                Label label = new Label(Integer.toString(i + j));
                label.setAlignment(Pos.CENTER);
                label.setId("stdLabel2");
                label.setPrefSize(50, 50);
                labelCollection.add(label);
                gridPane3.addColumn(j, label);
                label.setOnMousePressed(mouseEvent -> fieldClicked(label.getText(), rb1.isSelected()));
            }
        }
        mainMenu.setOnAction(this::openMenuChoice);
        shipLength.setText(Integer.toString(((Ship) currentShipToSet).getLength()));
        rotate.setOnAction(actionEvent -> {
            ((Ship) currentShipToSet).setShipPositions(markedCoordinates);
            String id = ShipToSet.getText().replace(" ", "");
            if (GameLogic.player.getFleet().searchForShipToRotate(id, GameLogic.player)) {
                nextShip.setDisable(false);
                rotated = true;
                setMarker(((Ship) currentShipToSet).getShipPositions(), false);
                markedCoordinates = ((Ship) currentShipToSet).getShipPositions();
                display.setText("You want to set your Ship at " + Arrays.toString(markedCoordinates));
            } else {
                display.setText("You cannot rotate Ship " + ((Ship) currentShipToSet).getId());
                for (int coordinatesToDelete : ((Ship) currentShipToSet).getShipPositions()) {
                    GameLogic.player.getHumanField().setFieldStatus(coordinatesToDelete, GameLogic.player.getHumanField().getFieldStatus(coordinatesToDelete));
                }
            }

        });
        rotate.setDisable(true);
    }

    /**
     * Method openMatchField will change the scene to the matchfield scene and will reset the scene by
     * calling the method clearAll.
     *
     * @param event needed for method.
     */
    @FXML
    private void openMatchField(ActionEvent event) {
        GameStart.getApplication().setScene("/fxml/matchfield.fxml", "Battleship");
        clearAll();
        log.info("Changed scene to matchfield scene!");
    }

    /**
     * Method openMenuChoice is opening the menu for the player to get back to main menu.
     *
     * @param actionEvent needed for method.
     */
    @FXML
    private void openMenuChoice(ActionEvent actionEvent) {
        menuNo.setOpacity(1);
        menuField.setOpacity(1);
        menuYes.setOpacity(1);
        toolBar.setOpacity(1);
        mainMenu.setOpacity(0);
        menuYes.setDisable(false);
    }

    /**
     * Method changeToMainMenu is changing the scene to the menu scene.
     *
     * @throws IOException if the menu scene is not there.
     */
    @FXML
    void changeToMainMenu() throws IOException {
        clearAll();
        GameStart.getApplication().setScene("/fxml/menu.fxml", "Menu");
        GameLogic.gamePrep();
        log.info("Changed scene back to main menu!");
    }

    /**
     * Method backToGame is hiding the back to main menu buttons if the player wanna
     * stay at the actual scene.
     */
    @FXML
    void backToGame() {
        menuNo.setOpacity(0);
        menuField.setOpacity(0);
        menuYes.setOpacity(0);
        toolBar.setOpacity(0);
        mainMenu.setOpacity(1);
        menuYes.setDisable(true);
    }

    /**
     * Method clearAll is resetting the scene so we can rerun the game.
     */
    private void clearAll() {
        gridPane3 = null;
        labelCollection.clear();
        markedCoordinates = null;
    }
}
