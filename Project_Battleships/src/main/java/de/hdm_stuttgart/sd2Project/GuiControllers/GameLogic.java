package de.hdm_stuttgart.sd2Project.GuiControllers;

import de.hdm_stuttgart.sd2Project.PlayerControl.ComputerPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GameLogic {

    private static final Logger log = LogManager.getLogger(GameLogic.class);

    static ComputerPlayer npc;
   public static HumanPlayer player;

    /**
     * Method gamePrep is creating a new HumanPlayer and ComputerPlayer and sets the ships of the Computer Player.
     */
    static void gamePrep() {
        long startTime = System.currentTimeMillis();
        Thread npcThread = new Thread(new NpcCreator());
        Thread humanThread = new Thread(new HumanCreator());
        npcThread.start();
        humanThread.start();
        try {
            npcThread.join();
            humanThread.join();
            log.debug("Total time: " + (System.currentTimeMillis() - startTime));
        } catch (InterruptedException threadInterruption) {
            log.error(threadInterruption.getMessage());
        }
    }

    private static class HumanCreator implements Runnable {
        @Override
        public void run() {
            player = new HumanPlayer();
        }
    }

    private static class NpcCreator implements Runnable {
        @Override
        public void run() {
            npc = new ComputerPlayer();
            npc.npcSetShips();
        }
    }
}

