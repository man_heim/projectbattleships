package de.hdm_stuttgart.sd2Project.GuiControllers;

import de.hdm_stuttgart.sd2Project.PlayerControl.Player;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;

import static de.hdm_stuttgart.sd2Project.GuiControllers.GameLogic.npc;
import static de.hdm_stuttgart.sd2Project.GuiControllers.GameLogic.player;
import static de.hdm_stuttgart.sd2Project.GuiControllers.GameStart.hm;

public class MatchFieldController implements Initializable {

    private static final Logger log = LogManager.getLogger(MatchFieldController.class);

    private Set<Button> buttonCollection = new HashSet<>();
    private Set<Label> labelCollection = new HashSet<>();
    private Set<Button> buttonHitCollection = new HashSet<>();
    private boolean readyToShoot = true;

    @FXML
    AnchorPane mainPage;
    @FXML
    GridPane aboveGrid2;
    @FXML
    GridPane aboveGrid;
    @FXML
    private GridPane gridPane1;
    @FXML
    private GridPane gridPane2;
    @FXML
    private Label shotCounter;
    @FXML
    private Label playerNameField;
    @FXML
    private Button mainMenu;
    @FXML
    private Button menuYes;
    @FXML
    private Button menuNo;
    @FXML
    private ToolBar toolBar;
    @FXML
    private Label menuField;
    @FXML
    private Label playerDisplay;
    @FXML
    private Label npcDisplay;
    @FXML
    private Label playerPoints;
    @FXML
    private Label computerPoints;
    @FXML
    private Button turretButton;
    @FXML
    private Button confirmTurret;
    @FXML
    private Button cancelTurret;

    public MatchFieldController() {
    }

    /**
     * Method markShips change the id of the label if there is a ship set.
     *
     * @param shipPositions which label id should be changed.
     */
    @FXML
    private void markShips(int[] shipPositions) {
        for (int i : shipPositions) {
            labelCollection.parallelStream()
                    .filter(label -> Integer.parseInt(label.getText()) == i)
                    .findAny()
                    .ifPresent((Label label) -> {
                        label.setId("labelWithShip");
                    });

        }
    }

    /**
     * Method getHumanShips gets the position of every ship of the HumanPlayer fleet.
     */
    private void getHumanShips() {
        for (IShip s : player.getFleet().getFleetList()) {
            markShips(((Ship) s).getShipPositions());
        }
    }

    /**
     * Method counter is changing the bombCounter after player click a Field.
     */
    private void counter() {
        shotCounter.setText(Integer.toString(player.getBombCounter()));
        log.debug("Changed bombCounter to " + (player.getBombCounter()));
    }

    /**
     * Method changeButtonColorShipField is changing the color of the button if the player hit a ship.
     *
     * @param fieldNr which button should be changed.
     */
    private void changeButtonColorShipField(int fieldNr) {
        buttonCollection.parallelStream()
                .filter(button -> (button.getText().equals(Integer.toString(fieldNr))))
                .findFirst()
                .ifPresent(this::setButtonColourHit);
    }

    /**
     * Method changeButtonColorMissedField is changing the color of the button if the player misses a ship.
     *
     * @param fieldNr which button should be changed.
     */
    private void changeButtonColorMissedField(int fieldNr) {
        buttonCollection.parallelStream()
                .filter(button -> (button.getText().equals(Integer.toString(fieldNr))))
                .findFirst()
                .ifPresent(this::setButtonColourMissed);
    }

    /**
     * Method changeLabelColorShipField is changing the color of the label if the computer hit a ship.
     *
     * @param fieldNr which button should be changed.
     */
    private void changeLabelColorShipField(int fieldNr) {
        labelCollection.parallelStream()
                .filter(label -> (label.getText().equals(Integer.toString(fieldNr))))
                .findFirst()
                .ifPresent(this::setLabelColorHit);
    }

    /**
     * Method changeLabelColorMissedField is changing the color of the label if the computer misses a ship.
     *
     * @param fieldNr which label should be changed.
     */
    private void changeLabelColorMissedField(int fieldNr) {
        labelCollection.
                parallelStream()
                .filter(label -> (label.getText().equals(Integer.toString(fieldNr))))
                .findFirst()
                .ifPresent(this::setLabelColorMissed);
    }

    /**
     * Method setButtonColorHit change the id so that the color changes.
     *
     * @param button which button id should be changed.
     */
    private void setButtonColourHit(Button button) {
        button.setId("buttonHit");
        button.setDisable(true);
        button.applyCss();
        buttonHitCollection.add(button);
        buttonCollection.remove(button);
    }

    /**
     * Method setButtonColorMissed change the id so that the color changes.
     *
     * @param button which button id should be changed.
     */
    private void setButtonColourMissed(Button button) {
        button.setId("buttonMissed");
        button.setDisable(true);
        button.applyCss();
        buttonHitCollection.add(button);
        buttonCollection.remove(button);
    }

    /**
     * Method setLabelColorHit change the id so that the color changes.
     *
     * @param label which label id should be changed.
     */
    private void setLabelColorHit(Label label) {
        label.setId("labelHit");
        label.applyCss();
    }

    /**
     * Method setLabelColorHit change the id so that the color changes.
     *
     * @param label which label id should be changed.
     */
    private void setLabelColorMissed(Label label) {
        label.setId("labelMissed");
        label.setDisable(true);
        label.applyCss();
    }

    /**
     * Method setWinOrLoseScreen sets the right scene if the computer or the player wins.
     */
    private void setWinOrLoseScreen() {

        if (player.isPlayerWin()) {
            hm.addScore(GameLogic.player.getName(), GameLogic.player.getBombCounter());
            clearAll();
            GameStart.getApplication().setScene("/fxml/win.fxml", "Battleship");
            log.info("Changed to win scene!");
        } else if (npc.isComputerWin()) {
            clearAll();
            GameStart.getApplication().setScene("/fxml/loose.fxml", "Battleship");
            log.info("Changed to loose scene!");
        }
    }

    /**
     * Method openMenuChoice is opening the menu if player wants back to main menu.
     *
     * @param actionEvent needed for method.
     */
    private void openMenuChoice(ActionEvent actionEvent) {
        menuNo.setOpacity(1);
        menuField.setOpacity(1);
        menuYes.setOpacity(1);
        toolBar.setOpacity(1);
        mainMenu.setOpacity(0);
        menuYes.setDisable(false);
    }

    /**
     * Method changeToMainMenu is changing the scene to the menu scene.
     **/
    @FXML
    void changeToMainMenu() {
        GameStart.getApplication().setScene("/fxml/menu.fxml", "Menu");
        clearAll();
        log.info("Changed scene back to main menu!");

    }

    /**
     * Method backToGame is hiding the menu if the player does not want back to main menu.
     */
    @FXML
    void backToGame() {
        menuNo.setOpacity(0);
        menuField.setOpacity(0);
        menuYes.setOpacity(0);
        toolBar.setOpacity(0);
        mainMenu.setOpacity(1);
        menuYes.setDisable(true);
    }

    /**
     * Method clearAll is clearing the scene if you want back to main menu or if someone wins the game.
     */
    private void clearAll() {
        GameLogic.gamePrep();
        player.setBombCounter(0);
        aboveGrid = null;
        gridPane1 = null;
        gridPane2 = null;
        buttonCollection.clear();
        labelCollection.clear();
    }


    /**
     * Method computerShootsField is choosing a random Number which the computer shots.
     * It will change the color if the computer misses or hit a Field.
     */
    private void computerShootsField() {
        boolean missed = false;
        while (!missed) {
            int labelNumber = GameLogic.npc.chooseFieldToShoot();
            if (GameLogic.npc.bombField(labelNumber, player) == 2) {
                changeLabelColorShipField(labelNumber);
                npcDisplay.setText("Computer hit Ship at Field " + labelNumber);
                log.info("Computer player hit ship at Field " + labelNumber);
                log.debug("Computer points are " + npc.getPoints());
                computerPoints.setText(String.valueOf(npc.getPoints()));

            } else {
                changeLabelColorMissedField(labelNumber);
                npcDisplay.setText("Computer missed at Field " + labelNumber);
                log.info("Computer player missed at Field " + labelNumber);
                missed = true;
            }
        }
    }


    /**
     * Method fieldHasBeenClicked is changing the color of the clicked Button if player hits or misses
     * a ship.
     *
     * @param clickedField which button has been clicked and which field must be changed.
     */
    private void fieldHasBeenClicked(int clickedField) {
        if (readyToShoot) {
            boolean missed = false;
            if (GameLogic.player.bombField(clickedField, GameLogic.npc) == 2) {
                changeButtonColorShipField(clickedField);
                playerDisplay.setText("You hit ship at Field " + clickedField);
                log.info(player.getName() + " hit ship at Field " + clickedField);
                System.out.println(player.getPoints());
                log.debug("Player points are: " + player.getPoints());
                playerPoints.setText(String.valueOf(player.getPoints()));
            } else {
                changeButtonColorMissedField(clickedField);
                playerDisplay.setText("You missed at Field " + clickedField);
                log.info(player.getName() + " missed at Field " + clickedField);
                missed = true;

            }
            letTurretsShoot();
            if (missed)
                computerShootsField();
        } else {
            playerDisplay.setText("Set your turret");
        }
    }

    private void letTurretsShoot() {
        player.getPlayersTurrets().forEach(turret -> {
            int fieldShot = turret.fieldToShoot();
            if (player.bombField(fieldShot, npc) == 2) {
                changeButtonColorShipField(fieldShot);
                playerDisplay.setText("Your turret hit ship at Field " + fieldShot);
                log.info("Turret hit ship at field " + fieldShot);
                playerPoints.setText(String.valueOf(player.getPoints()));
            } else {
                changeButtonColorMissedField(fieldShot);
                playerDisplay.setText("Turret missed Field at " + fieldShot);
                log.info(player.getName() + " missed at Field " + fieldShot);
            }
        });
    }
    // try {
    //     Thread computerHelper = new Thread(new ComputerShotHelper());
    //    computerHelper.start();                                         //TODO Wie verzögerung in Thread Computer Helper
    //   computerHelper.wait(2000);
    //  computerHelper.join();
    //   } catch (InterruptedException interrupt) {
    //       log.error("Error with the Computer Thread " + interrupt.getMessage());
    //   }


    private void openBuyTurret() {

        if (player.getPoints() < 2) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Shop");
            alert.setContentText("You need at least 2 points to set a turret");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Shop");
            alert.setContentText("Do you want to buy a turret? \n This will cost 2 of your points");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                log.debug("Player bought a turret");
                playerDisplay.setText("You bought a turret, please set it \n on any field you have already bombed");
                playerDisplay.setStyle("-fx-font-size: 18px");
                player.setPoints(player.getPoints() - 2);
                playerPoints.setText(String.valueOf(player.getPoints()));
                settingTurretPosition();
                computerShootsField();
            } else {
                log.debug("Player bought no turret");
            }
        }
    }

    void settingTurretPosition() {
        readyToShoot = false;
        buttonCollection.forEach(button -> button.setDisable(true));
        buttonHitCollection.forEach(button -> {
            if (button.getId().equals("buttonMissed")) {
                button.setDisable(false);
                button.setOnMouseClicked(actionEvent -> {
                    //  cancelTurret.setVisible(true);//TODO cancel button muss noch gemacht werden
                    //   cancelTurret.setDisable(false);
                    confirmTurret.setVisible(true);
                    confirmTurret.setDisable(false);
                    player.createTurret(button.getText());
                    buttonCollection.forEach(button1 -> {
                        button1.setDisable(true);
                        if (player.getPlayersTurrets().get(player.getPlayersTurrets().size() - 1).getTurretRange().contains(Integer.parseInt(button1.getText()))) {
                            button1.setId("markedTurret");
                        }
                    });
                    buttonHitCollection.forEach(button1 -> {
                        button1.setDisable(true);
                        if ((!button1.getText().equals("")) && player.getPlayersTurrets().get(player.getPlayersTurrets().size() - 1).getTurretRange().contains(Integer.parseInt(button1.getText()))) {
                            if (button1.getId().equals("buttonHit")) button1.setId("markedFormerHit");
                            if (button1.getId().equals("buttonMissed")) button1.setId("markedFormerMissed");
                        }
                    });
                    playerDisplay.setText("Confirm or Cancel Turret Position");
                    button.setId("turret");
                    button.setText("");
                    ;

                });
            }
        });
    }


    @FXML
    void cancelTurret() {
        player.getPlayersTurrets().remove(player.getPlayersTurrets().size() - 1);
        readyToShoot = true;
        player.setPoints(player.getPoints() + 2);

    }

    @FXML
    void confirmTurret() {
        readyToShoot = true;
        buttonHitCollection.forEach(button -> {
            button.setDisable(true);
            if (button.getId().equals("markedFormerHit"))
                button.setId("buttonHit");
            if (button.getId().equals("markedFormerMissed"))
                button.setId("buttonMissed");
        });
        buttonCollection.forEach(button -> {
            button.setDisable(false);
            button.setId("stdButton");
        });
        confirmTurret.setDisable(true);
        confirmTurret.setVisible(false);
        cancelTurret.setVisible(false);
        cancelTurret.setDisable(true);
        computerShootsField();
    }


    /**
     * Method initialize is setting a button in every pane of the first gridpane and a label in the second pane.
     * For every button it will activate method fieldHasBeenClicked.
     * It sets the MatchField names and activate back to main menu.
     *
     * @param url            needed for this method.
     * @param resourceBundle needed for this method.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (int i = 0; i < 100; i += 10) {
            gridPane1.addRow(i);
            for (int j = 1; j < 11; j++) {
                Button button = new Button(Integer.toString(i + j));
                button.setPrefSize(50, 50);
                button.setOnAction(actionEvent -> {
                    fieldHasBeenClicked(Integer.parseInt(button.getText()));
                    counter();
                    setWinOrLoseScreen();
                });
                gridPane1.addColumn(j, button);
                buttonCollection.add(button);
                Label label = new Label(Integer.toString(i + j));
                label.setAlignment(Pos.CENTER);
                label.setId("stdLabel");
                label.setPrefSize(50, 50);
                labelCollection.add(label);
                gridPane2.addColumn(j, label);

            }
        }
        if (player.getName().endsWith("s")) {
            playerNameField.setText(player.getName() + "' Field");
        } else {
            playerNameField.setText(player.getName() + "'s Field");
        }
        mainMenu.setOnAction(this::openMenuChoice);
        getHumanShips();
        turretButton.setOnMouseClicked(actionEvent -> {
            openBuyTurret();
        });
    }


}
