package de.hdm_stuttgart.sd2Project.GuiControllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

import static de.hdm_stuttgart.sd2Project.GuiControllers.GameStart.hm;

public class ScoreListController implements Initializable {

    private static final Logger log = LogManager.getLogger(MenuController.class);

    @FXML
    private Label scoreList;

    /**
     * Method initialize is adding ScoreList to scene.
     *
     * @param url            needed for method.
     * @param resourceBundle needed for method.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        scoreList.setText(hm.getHighScoreString());
    }

    /**
     * Method backToMainMenu is changing scene back to main menu.
     *
     * @param event needed for method.
     */
    @FXML
    private void backToMainMenu(ActionEvent event) {
        GameStart.getApplication().setScene("/fxml/menu.fxml", "Battleship");
        log.info("Change to menu scene!");
    }

    /**
     * Method clearList is setting/clearing the label of the ScoreList.
     */
    @FXML
    private void clearList() {
        hm.clearScoreList();
        scoreList.setText(hm.getHighScoreString());
        log.debug("ScoreList got cleared!");
    }
}
