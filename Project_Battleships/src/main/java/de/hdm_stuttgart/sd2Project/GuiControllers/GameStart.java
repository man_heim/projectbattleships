package de.hdm_stuttgart.sd2Project.GuiControllers;

import de.hdm_stuttgart.sd2Project.HighScore.HighScoreManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GameStart extends Application {

    private static final Logger log = LogManager.getLogger(GameStart.class);
    static HighScoreManager hm;
    private static GameStart application;
    private static Stage stage = null;

    /**
     * Method getApplication.
     *
     * @return the application we started.
     */
    static GameStart getApplication() {
        return application;
    }

    /**
     * Method Main is launching the game.
     *
     * @param args needed to set Thread.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Method start sets the first stage and scene and is calling gamePrep to create the players.
     *
     * @param stage first stage which will be created.
     */
    @Override
    public void start(Stage stage) {
        hm = new HighScoreManager();
        GameLogic.gamePrep();
        application = this;
        GameStart.stage = stage;
        setScene("/fxml/menu.fxml", "Menu");
        log.info("Menu scene loaded!");
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Method setScene is load a new scene and set the scene at the stage.
     * If the files are not correct and a scene is missing, the user sees an alert notification
     *
     * @param fxml  which fxml-file (scene) should be used.
     * @param title which title the stage should have.
     */
    void setScene(String fxml, String title) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxml));
            Scene scene = new Scene(root, 1500, 900);
            stage.setScene(scene);
            stage.setTitle(title);
        } catch (Exception resourceExc) {
            log.fatal("Resource Problem with the files, they might be not complete or damaged \n" + resourceExc.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fatal Error");
            alert.setHeaderText("Error with your files");
            alert.setContentText("Your files might be damaged or not complete \n" +
                    "See your log-file for more information \n" +
                    "The Application will be closed");
            ButtonType closeButton = new ButtonType("close");
            alert.getButtonTypes().clear();
            alert.getButtonTypes().add(closeButton);
            alert.showAndWait();
            if (!alert.isShowing()) {
                System.exit(1);
            }
        }
    }
}
