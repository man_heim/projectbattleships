package de.hdm_stuttgart.sd2Project.GuiControllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {

    private static final Logger log = LogManager.getLogger(MenuController.class);

    @FXML
    private Button submitName;
    @FXML
    private Button startGame;
    @FXML
    private TextField nameInput;
    private String playerName;

    /**
     * Method initialize will set buttons on start.
     *
     * @param url            needed for method.
     * @param resourceBundle needed for method.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        submitName.setOnAction(this::submitName);
        nameInput.setId("nameNotSubmitted");
        startGame.setDisable(true);
    }

    /**
     * Method openSetShips is changing the scene to setShips scene.
     *
     * @param event needed for method.
     */
    @FXML
    private void openSetShips(ActionEvent event) {
        GameStart.getApplication().setScene("/fxml/setShips.fxml", "Battleship");
        log.info("Changed scene to setShips!");
    }

    /**
     * Method exit will close the game if the corresponding button is pressed.
     *
     * @param event needed for method.
     */
    @FXML
    private void exit(ActionEvent event) {
        log.info("Close Game!");
        System.exit(0);
    }

    /**
     * Method setName will set the name of the player.
     */
    private void setName() {
        playerName = nameInput.getText();
    }

    /**
     * Method submitName will set call setName and will change the name to the input the player gave.
     *
     * @param event needed for method.
     */
    @FXML
    private void submitName(ActionEvent event) {
        setName();
        GameLogic.player.setName(playerName);
        nameInput.setId("nameSubmitted");
        checkIfNameSet();
        nameInput.setDisable(true);
        submitName.setDisable(true);
    }

    /**
     * Method checkIfNameSet looks if a name is set and will enable the button to start the game.
     */
    private void checkIfNameSet() {
        if (nameInput != null) {
            startGame.setDisable(false);
        }
    }

    /**
     * Method openHighScore is changing the scene to the ScoreList scene.
     *
     * @param event needed for method.
     */
    @FXML
    private void openHighScore(ActionEvent event) {
        GameStart.getApplication().setScene("/fxml/ScoreList.fxml", "Battleship");
        log.info("Change to ScoreList scene!");
    }
}
