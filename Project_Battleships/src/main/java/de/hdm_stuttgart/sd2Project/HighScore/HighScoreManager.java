package de.hdm_stuttgart.sd2Project.HighScore;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;

public class HighScoreManager {

    private static final Logger log = LogManager.getLogger(HighScoreManager.class);

    private static final String HIGHSCORE_FILE = "scores.txt";
    private ArrayList<ScoreEntry> scores;

    /**
     * HighScoreManager made a new ArrayList of ScoreEntries.
     */
    public HighScoreManager() {
        scores = new ArrayList<>();
    }

    /**
     * Method getScores is loading and sorting the ScoreList.
     *
     * @return ArrayList of entries.
     */
    private ArrayList<ScoreEntry> getScores() {
        File file = new File(HIGHSCORE_FILE);
        if (file.length() > 0) {
            loadScoreFile();
            sort();
        }
        return scores;
    }

    /**
     * Method sort is sorting the ArrayList with the compareTO-method implemented in ScoreEntry.
     */
    private void sort() {
        scores.sort(ScoreEntry::compareTo);
    }

    /**
     * Method addScore is adding a new ScoreEntry to the ArrayList.
     *
     * @param name  of entry.
     * @param score of entry.
     */
    public void addScore(String name, int score) {
        scores.add(new ScoreEntry(name, score));
        updateScoreFile();
        log.info("New entry added!");
    }

    /**
     * Method loadScoreFile is reading the score.txt and updates the ArrayList scores.
     */
    private void loadScoreFile() {
        try (FileInputStream fIn = new FileInputStream(HIGHSCORE_FILE)) {
            try (ObjectInputStream in = new ObjectInputStream(fIn)) {
                scores = (ArrayList<ScoreEntry>) in.readObject();
            }
        } catch (IOException | ClassNotFoundException e) {
            log.warn(e.getMessage());
        }
    }

    /**
     * Method updateScoreFile is writing a new entry in the score.txt file.
     */
    private void updateScoreFile() {
        try (FileOutputStream fOut = new FileOutputStream(HIGHSCORE_FILE); ObjectOutput out = new ObjectOutputStream(fOut)) {
            out.writeObject(scores);
            out.flush();
        } catch (IOException e) {
            log.warn(e.getMessage() + "Possible problem with Highscore-File");
        }
    }

    /**
     * Method getHighScoreString is creating a String of all entries.
     *
     * @return String of all entries.
     */
    public String getHighScoreString() {
        StringBuilder highScoreString = new StringBuilder();
        int max = 10;
        int maxGap = 11;

        ArrayList<ScoreEntry> scores;
        scores = getScores();

        int i = 0;
        int x = scores.size();
        if (x > max) {
            x = max;
        }
        while (i < x) {
            int length = scores.get(i).getName().length();
            int t = maxGap - length;
            highScoreString.append(i + 1).append(".\t");
            highScoreString.append(scores.get(i).getName());
            highScoreString.append("\t".repeat(t));
            highScoreString.append(scores.get(i).getScore()).append("\n");
            i++;
        }
        return highScoreString.toString();
    }

    /**
     * Method clearScoreList is clearing the ScoreList.
     */
    public void clearScoreList() {
        try (PrintWriter writer = new PrintWriter(new FileWriter(HIGHSCORE_FILE))) {
            writer.flush();
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        scores.clear();
        log.info("Score list got cleared!");
    }
}

