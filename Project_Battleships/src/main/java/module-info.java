module de.hdm_stuttgart.sd2Project {
    requires javafx.controls;
    requires javafx.fxml;
    requires log4j.api;

    opens de.hdm_stuttgart.sd2Project.GuiControllers to javafx.fxml, log4j.api;
    exports de.hdm_stuttgart.sd2Project.GuiControllers;
}