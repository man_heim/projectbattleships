package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.MapControl.Field;
import de.hdm_stuttgart.sd2Project.MapControl.MatchField;
import de.hdm_stuttgart.sd2Project.PlayerControl.ComputerPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.IPlayer;
import org.junit.Assert;
import org.junit.Test;

public class TestFieldConstruction {

    private IPlayer humanPlayer = new HumanPlayer();
    private IPlayer computerPlayer = new ComputerPlayer();

    @Test
    public void testFieldConstruction() {        //This test is horrible
        MatchField m1 = new MatchField();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Assert.assertTrue(m1.getMatchField()[i][j] instanceof Field && ((HumanPlayer) humanPlayer).getHumanField().getMatchField()[i][j] instanceof Field);
            }
        }
    }

    @Test
    public void fieldsDiffer() {
        Field[][] humanField = ((HumanPlayer) humanPlayer).getHumanField().getMatchField();
        Field[][] computerField = ((ComputerPlayer) computerPlayer).getComputerField().getMatchField();
        Assert.assertNotSame(humanField, computerField);

    }

    @Test
    public void field_StatusTest() {
        Assert.assertTrue((0 == ((HumanPlayer) humanPlayer).getHumanField().getFieldStatus(12)));
        ((HumanPlayer) humanPlayer).getHumanField().setFieldStatus(12, 1);
        Assert.assertTrue((1 == ((HumanPlayer) humanPlayer).getHumanField().getFieldStatus(12)));
        Assert.assertEquals(10, ((HumanPlayer) humanPlayer).getHumanField().getMatchField()[0][9].getFieldNumber());
        Assert.assertEquals(1, ((HumanPlayer) humanPlayer).getHumanField().getMatchField()[0][0].getFieldNumber());
        Assert.assertEquals(36, ((HumanPlayer) humanPlayer).getHumanField().getMatchField()[3][5].getFieldNumber());
        Assert.assertEquals(58, ((HumanPlayer) humanPlayer).getHumanField().getMatchField()[5][7].getFieldNumber());

    }


}
