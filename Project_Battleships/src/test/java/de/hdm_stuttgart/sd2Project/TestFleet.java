package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.Exceptions.IllegalFactoryArgument;
import de.hdm_stuttgart.sd2Project.ShipCollection.*;
import org.junit.Assert;
import org.junit.Test;

public class TestFleet {

    @Test
    public void testShipFactory() {

        try {
            Assert.assertTrue(ShipFactory.createShip("Destroyer") instanceof Destroyer);
            Assert.assertTrue(ShipFactory.createShip("AircraftCarrier") instanceof AircraftCarrier);
            Assert.assertTrue(ShipFactory.createShip("Cruiser") instanceof Cruiser);
            Assert.assertTrue(ShipFactory.createShip("Submarine") instanceof Submarine);

        } catch (IllegalFactoryArgument illegalFactoryArgument) {
            illegalFactoryArgument.printStackTrace();
        }
    }


    @Test(expected = IllegalFactoryArgument.class)
    public void testShipFactoryFail() throws IllegalFactoryArgument {
        try {
            ShipFactory.createShip("sdfhusdfh df 123");
        } catch (IllegalFactoryArgument illegalFactoryArgument) {
            illegalFactoryArgument.printStackTrace();
            throw illegalFactoryArgument;
        }
    }
}
