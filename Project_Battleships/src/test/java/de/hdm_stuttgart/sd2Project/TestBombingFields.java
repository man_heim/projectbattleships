package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestBombingFields {

    private static HumanPlayer xyz = new HumanPlayer();
    private static HumanPlayer secondHumanPlayer = new HumanPlayer();

    @BeforeClass
    public static void setExampleShips() {
        secondHumanPlayer.setShips(new int[]{1, 2, 3, 4, 5}, "AircraftCarrier");
        secondHumanPlayer.setShips(new int[]{44, 45, 46, 47}, "Cruiser1");
        secondHumanPlayer.setShips(new int[]{64, 65, 66, 67}, "Cruiser2");
        secondHumanPlayer.setShips(new int[]{80, 90, 100}, "Destroyer1");
        secondHumanPlayer.setShips(new int[]{19, 29, 39}, "Destroyer2");
        secondHumanPlayer.setShips(new int[]{85, 86, 87}, "Destroyer3");
        secondHumanPlayer.setShips(new int[]{7, 8}, "Submarine1");
        secondHumanPlayer.setShips(new int[]{91, 92}, "Submarine2");
        secondHumanPlayer.setShips(new int[]{13, 23}, "Submarine3");
        secondHumanPlayer.setShips(new int[]{50, 60}, "Submarine4");
    }

    @Test
    public void bombingWater() {
        xyz.bombField(6, secondHumanPlayer);
        Assert.assertEquals(3, secondHumanPlayer.getHumanField().getFieldStatus(6));

        xyz.bombField(99, secondHumanPlayer);
        Assert.assertEquals(3, secondHumanPlayer.getHumanField().getFieldStatus(99));

        xyz.bombField(38, secondHumanPlayer);
        Assert.assertEquals(3, secondHumanPlayer.getHumanField().getFieldStatus(38));
    }

    @Test
    public void bombingShip() {
        xyz.bombField(1, secondHumanPlayer);
        Assert.assertEquals(2, secondHumanPlayer.getHumanField().getFieldStatus(1));

        xyz.bombField(100, secondHumanPlayer);
        Assert.assertEquals(2, secondHumanPlayer.getHumanField().getFieldStatus(100));
    }

    @Test
    public void bombingFieldAgain() {
        xyz.bombField(1, secondHumanPlayer);
        xyz.bombField(1, secondHumanPlayer);
        Assert.assertEquals(2, secondHumanPlayer.getHumanField().getFieldStatus(1));

        xyz.bombField(99, secondHumanPlayer);
        xyz.bombField(99, secondHumanPlayer);
        Assert.assertEquals(3, secondHumanPlayer.getHumanField().getFieldStatus(99));
    }
}
