package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestRotatingShips {

    private static HumanPlayer xyz = new HumanPlayer();

    @BeforeClass
    public static void setExampleShips() {
        xyz.setShips(new int[]{1, 2, 3, 4, 5}, "AircraftCarrier");
        xyz.setShips(new int[]{34, 44, 54, 64}, "Cruiser1");
        xyz.setShips(new int[]{80, 90, 100}, "Destroyer1");
        xyz.setShips(new int[]{9, 19, 29}, "Destroyer2");
        // xyz.setShips(new int[]{55,56,57},"Destroyer3",xyz);
    }

    /**
     * Testing the rotation with horizontal ship, the field status on the positions has to be set to
     * 0 to fake marking of coordinates (coordinates are only marked, not yet setted)
     * Cannot test for the value of the fields after the rotation, because the coordinates only get marked
     * and not yet setted. In the game the fields get setted only when the player chooses to proceed to the next ship
     */
    @Test
    public void rotatingShipHorizontal() {
        IShip s = xyz.getFleet().getFleetList().get(0);
        for (int i : ((Ship) s).getShipPositions()) {
            xyz.getHumanField().setFieldStatus(i, 0);
        }
        s.rotateShip(xyz);
        Assert.assertArrayEquals(((Ship) s).getShipPositions(), new int[]{1, 11, 21, 31, 41});
    }

    /**
     * Testing the rotation with horizontal ship, the field status on the positions has to be set to
     * 0 to fake marking of coordinates (coordinates are only marked, not yet setted)
     * -------How the test works can be seen in the comment of the above test-----------------
     */
    @Test
    public void rotatingShipVertical() {
        IShip s = xyz.getFleet().getFleetList().get(1);
        for (int i : ((Ship) s).getShipPositions()) {
            xyz.getHumanField().setFieldStatus(i, 0);
        }
        xyz.getFleet().searchForShipToRotate("Cruiser1", xyz);
        Assert.assertArrayEquals(((Ship) s).getShipPositions(), new int[]{34, 35, 36, 37});

    }

    @Test
    public void rotatingShipNotPossible() {
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(80));
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(90));
        xyz.getFleet().searchForShipToRotate("Destroyer1", xyz);
        //Positions are the same!
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(80));
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(90));

        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(19));
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(29));
        xyz.getFleet().searchForShipToRotate("Destroyer2", xyz);        //Positions are the same!
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(19));
        Assert.assertEquals(1, xyz.getHumanField().getFieldStatus(29));
    }


}
