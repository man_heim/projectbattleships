package de.hdm_stuttgart.sd2Project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestFieldConstruction.class, TestSettingShips.class, TestFleet.class, TestBombingFields.class, TestRotatingShips.class, TestReducingHealthPoints.class})

public class SuitTest {
    //empty
}

