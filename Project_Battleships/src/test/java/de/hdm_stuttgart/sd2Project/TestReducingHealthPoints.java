package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.PlayerControl.ComputerPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.Player;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestReducingHealthPoints {

    private static Player xyz = new ComputerPlayer();
    private static HumanPlayer secondHumanPlayer = new HumanPlayer();

    @BeforeClass
    public static void setFleetNPC() {
        //All ships have to be set before you can shoot a field!
        secondHumanPlayer.setShips(new int[]{1, 11, 21, 31, 41}, "AircraftCarrier");
        secondHumanPlayer.setShips(new int[]{44, 45, 46, 47}, "Cruiser1");
        secondHumanPlayer.setShips(new int[]{64, 65, 66, 67}, "Cruiser2");
        secondHumanPlayer.setShips(new int[]{80, 90, 100}, "Destroyer1");
        secondHumanPlayer.setShips(new int[]{19, 29, 39}, "Destroyer2");
        secondHumanPlayer.setShips(new int[]{85, 86, 87}, "Destroyer3");
        secondHumanPlayer.setShips(new int[]{7, 8}, "Submarine1");
        secondHumanPlayer.setShips(new int[]{91, 92}, "Submarine2");
        secondHumanPlayer.setShips(new int[]{13, 23}, "Submarine3");
        secondHumanPlayer.setShips(new int[]{50, 60}, "Submarine4");
    }

    @Test
    public void reducingHealthShip() {
        xyz.bombField(1, secondHumanPlayer);
        Assert.assertEquals(4, ((Ship) secondHumanPlayer.getFleet().getFleetList().get(0)).getHealthPoints());
        xyz.bombField(11, secondHumanPlayer);
        Assert.assertEquals(3, ((Ship) secondHumanPlayer.getFleet().getFleetList().get(0)).getHealthPoints());

        xyz.bombField(91, secondHumanPlayer);
        Assert.assertEquals(1, ((Ship) secondHumanPlayer.getFleet().getFleetList().get(7)).getHealthPoints());

        xyz.bombField(100, secondHumanPlayer);
        Assert.assertEquals(2, ((Ship) secondHumanPlayer.getFleet().getFleetList().get(3)).getHealthPoints());

        xyz.bombField(45, secondHumanPlayer);
        Assert.assertEquals(3, ((Ship) secondHumanPlayer.getFleet().getFleetList().get(1)).getHealthPoints());

    }

    @Test
    public void removeDestroyedShipFromFleet() {
        xyz.bombField(1, secondHumanPlayer);
        xyz.bombField(11, secondHumanPlayer);
        xyz.bombField(21, secondHumanPlayer);
        xyz.bombField(31, secondHumanPlayer);
        xyz.bombField(41, secondHumanPlayer);
        Assert.assertEquals(9, secondHumanPlayer.getFleet().getFleetList().size());

        xyz.bombField(13, secondHumanPlayer);
        xyz.bombField(23, secondHumanPlayer);
        Assert.assertEquals(8, secondHumanPlayer.getFleet().getFleetList().size());

        xyz.bombField(19, secondHumanPlayer);
        xyz.bombField(29, secondHumanPlayer);
        xyz.bombField(39, secondHumanPlayer);
        Assert.assertEquals(7, secondHumanPlayer.getFleet().getFleetList().size());

    }
}
