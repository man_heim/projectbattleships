package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.PlayerControl.HumanPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.IPlayer;
import de.hdm_stuttgart.sd2Project.PlayerControl.Player;
import de.hdm_stuttgart.sd2Project.ShipCollection.IShip;
import de.hdm_stuttgart.sd2Project.ShipCollection.Ship;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestSettingShips {

    private static IPlayer xyz = new HumanPlayer();
    private static IShip exampleShip;
    private static IShip secondExampleShip;


    @BeforeClass
    public static void setHorizontalShip() {
        exampleShip = xyz.getFleet().getFleetList().get(0);
        xyz.setShips(new int[]{1, 2, 3, 4, 5}, "AircraftCarrier");
    }

    @BeforeClass
    public static void setVerticalShip() {
        secondExampleShip = xyz.getFleet().getFleetList().get(3);
        xyz.setShips(new int[]{80, 90, 100}, "Destroyer1");
    }

    @Test
    public void testingHorizontalShipCoordinates() {
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, ((Ship) exampleShip).getShipPositions());
        for (byte i = 1; i < 6; i++) {
            Assert.assertEquals(((HumanPlayer) xyz).getHumanField().getFieldStatus(i), 1);
        }
    }

    @Test
    public void testVerticalShipCoordinates() {
        Assert.assertArrayEquals(new int[]{80, 90, 100}, ((Ship) secondExampleShip).getShipPositions());
        for (byte i = 80; i <= 100; i += 10) {
            Assert.assertEquals(((HumanPlayer) xyz).getHumanField().getFieldStatus(i), 1);
        }
    }

    @Test
    public void testFalseID() {
        IShip localExample = xyz.getFleet().getFleetList().get(2);
        xyz.setShips(new int[]{21, 22, 23}, "ABCD");
        Assert.assertNull(((Ship) localExample).getShipPositions());
    }

    @Test
    public void testHorizontalTooClose() {

        int[] wantedPositions = new int[]{11, 12, 13, 14};
        int[] secondWantedPositions = new int[]{6, 7, 8, 9};
        IShip localExample = xyz.getFleet().getFleetList().get(2);
        xyz.setShips(wantedPositions, "Cruiser2");
        Assert.assertFalse(((Player) xyz).checkInput(wantedPositions, xyz));
        Assert.assertNull(((Ship) localExample).getShipPositions());
        xyz.setShips(secondWantedPositions, "Cruiser2");
        Assert.assertFalse(((Player) xyz).checkInput(wantedPositions, xyz));
        Assert.assertNull(((Ship) localExample).getShipPositions());
    }

    @Test
    public void testVerticalTooClose() {

        int[] wantedPositions = new int[]{15, 25, 35};
        int[] secondWantedPositions = new int[]{79, 89, 100};
        IShip localExample = xyz.getFleet().getFleetList().get(4);
        xyz.setShips(wantedPositions, "Destroyer2");
        Assert.assertFalse(((Player) xyz).checkInput(wantedPositions, xyz));
        Assert.assertNull(((Ship) localExample).getShipPositions());
        xyz.setShips(secondWantedPositions, "Destroyer2");
        Assert.assertFalse(((Player) xyz).checkInput(wantedPositions, xyz));
        Assert.assertNull(((Ship) localExample).getShipPositions());
    }

    @Test
    public void testShipCornerSet() {
        IShip localExample = xyz.getFleet().getFleetList().get(1);
        int[] wantedPositions = {66, 67, 68, 69};
        xyz.setShips(wantedPositions, "Cruiser1");
        for (byte i = 66; i < 70; i++) {
            Assert.assertEquals(((HumanPlayer) xyz).getHumanField().getFieldStatus(i), 1);
        }
        Assert.assertArrayEquals(wantedPositions, ((Ship) localExample).getShipPositions());
    }


}
