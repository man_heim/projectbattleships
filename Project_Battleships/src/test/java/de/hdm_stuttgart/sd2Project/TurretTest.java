package de.hdm_stuttgart.sd2Project;

import de.hdm_stuttgart.sd2Project.PlayerControl.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TurretTest {

    private static List<Integer> comparer;
    private static List<Integer> comparer2;


    @BeforeClass
    public static void createComparer() {
        comparer = new ArrayList<>();
        for (int i = 33; i <= 73; i += 10) {
            for (int j = 0; j < 5; j++) {
                comparer.add(i + j);
            }
        }
        comparer.add(25);
        comparer.add(52);
        comparer.add(58);
        comparer.add(85);
        comparer2 = new ArrayList<>();

        for (int i = 31; i <= 71; i += 10) {
            for (int j = 0; j < 3; j++) {
                comparer2.add(i + j);
            }
        }
        comparer2.add(21);
        comparer2.add(54);
        comparer2.add(81);

    }

    @Test
    public void testCreatingTurrets() {

        ITurret turret = TurretFactory.createTurret("55", "std");
        List<Integer> turretFields = turret.getTurretRange();
        Assert.assertEquals(comparer, turretFields);
    }

    @Test
    public void testCreatingTurretsViaPlayer() {
        IPlayer player = new HumanPlayer();
        ((Player) player).createTurret("55");
        List<Integer> turretFields = player.getPlayersTurrets().get(0).getTurretRange();
        Assert.assertEquals(turretFields, comparer);

    }

    @Test
    public void testCreatingTurretBorderLeft() {
        ITurret turret = TurretFactory.createTurret("51", "std");
        List<Integer> turretFields = turret.getTurretRange();
        Assert.assertEquals(comparer2, turretFields);

    }

    @Test
    public void testCreatingTurretBorderRight() {
        List<Integer> compare = new ArrayList<>();
        for (int i = 28; i <= 68; i += 10) {
            for (int j = 0; j < 3; j++) {
                compare.add(i + j);
            }
        }
        compare.add(20);
        compare.add(47);
        compare.add(80);
        ITurret turret = TurretFactory.createTurret("50", "std");
        List<Integer> turretFields = turret.getTurretRange();
        Assert.assertEquals(compare, turretFields);
    }

    @Test
    public void testCreatingBorderRight2() {
        List<Integer> compare = new ArrayList<>();
        for (int i = 8; i <= 38; i += 10) {
            for (int j = 0; j < 3; j++) {
                compare.add(i + j);
            }
        }
        compare.add(17);
        compare.add(50);
        ITurret turret = TurretFactory.createTurret("20", "std");
        List<Integer> turretFields = turret.getTurretRange();
        Assert.assertEquals(compare, turretFields);
    }
}

